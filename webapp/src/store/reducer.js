import {
    CHANGE_LAST_DATE,
    FETCH_MESSAGES_ERROR,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS
} from "./actions/actions";

const initialState = {
    loading: false,
    messages: [],
    lastDate: "",
    error: null,
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_REQUEST:
            return {...state, loading: true};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.messages, loading: false};
        case FETCH_MESSAGES_ERROR:
            return {...state, error: action.error, loading: false};
        case CHANGE_LAST_DATE:
            return {...state, lastDate: action.date};
        default:
            return state;
    }
};

export default reducer;