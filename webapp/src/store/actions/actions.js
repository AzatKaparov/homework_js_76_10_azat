export const FETCH_MESSAGES_REQUEST = "FETCH_MESSAGES_REQUEST";
export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const FETCH_MESSAGES_ERROR = "FETCH_MESSAGES_ERROR";
export const CHANGE_LAST_DATE = "CHANGE_LAST_DATE";

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = data => ({type: FETCH_MESSAGES_SUCCESS, messages: data});
export const fetchMessagesError = err => ({type: FETCH_MESSAGES_ERROR, error: err});
export const changeLastDate = date => ({type: CHANGE_LAST_DATE, date: date});

