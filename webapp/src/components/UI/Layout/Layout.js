import React from 'react';
import './Layout.css';

const Layout = ({children}) => {
  return (
    <>
      <main className="Layout-Content">
        {children}
      </main>
    </>
  );
};

export default Layout;