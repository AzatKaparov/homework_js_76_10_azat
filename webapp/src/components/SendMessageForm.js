import React from 'react';

const SendMessageForm = ({ submit, inpValue, onInpChange }) => {
    return (
        <div className="send-message-form">
            <form id="sendForm" action="" onSubmit={submit}>
                <input
                    onChange={onInpChange}
                    className="send-message-inp"
                    type="text" value={inpValue}
                    placeholder="Enter your message..."
                />
                <button
                    type="submit"
                    className="send-message-btn"
                >
                    Send
                </button>
            </form>
        </div>
    );
};

export default SendMessageForm;