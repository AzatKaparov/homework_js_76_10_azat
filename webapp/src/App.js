import './App.css'
import Messages from "./containers/Messages";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";

function App() {
  return (
    <div className="container">
        <Layout>
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Messages}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </BrowserRouter>
        </Layout>
    </div>
  );
}

export default App;
