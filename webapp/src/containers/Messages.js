import React, {useState, useEffect} from 'react';
import "./Messages.css";
import MessageItem from "../components/MessageItem";
import SendMessageForm from "../components/SendMessageForm";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {changeLastDate, fetchMessagesError, fetchMessagesRequest, fetchMessagesSuccess} from "../store/actions/actions";
import Modal from "../components/UI/Modal/Modal";
import Preloader from "../components/UI/Preloader/Preloader";
import Backdrop from "../components/UI/Backdrop/Backdrop";


const Messages = () => {
    const dispatch = useDispatch();
    const {lastDate, messages, loading, error} = useSelector(state => state);
    const [sendMessage, setSendMessage] = useState('');
    const url = "http://localhost:4321/messages";

    const handleSendMessage = e => {
        setSendMessage(e.target.value);
    };

    const doSendMessage = async e => {
        e.preventDefault();
        const data = {author: "Azat", message: sendMessage};
        const response = await axios.post('http://localhost:4321/messages', data);
        setSendMessage('');
        if (response.ok) {
            console.log("Message sent successfully");
        }
    };

    useEffect(() => {
        dispatch(fetchMessagesRequest());
        const fetchMessages = async () => {
            try {
                const response = await axios.get(`${url}?datetime=${lastDate}`);
                const data = response.data;
                if (data.length !== 0) {
                    dispatch(changeLastDate(data[data.length - 1].datetime));
                }
                dispatch(fetchMessagesSuccess(data));
            } catch (e) {
                dispatch(fetchMessagesError(e))
            }
        };
        const interval = setInterval(fetchMessages, 3000);
        return () => clearInterval(interval);
    }, []);

    return (
        <>
            <Modal show={error !== null}>
                {error}
            </Modal>
            <Preloader show={loading} />
            <Backdrop show={loading}/>
            <div className="messages">
            <h1>Welcome to our chat!</h1>
            <SendMessageForm
                inpValue={sendMessage}
                onInpChange={handleSendMessage}
                submit={doSendMessage}
            />
            <div className="messages-block">
                {messages.map(item => {
                    return (
                        <MessageItem
                            key={item.id}
                            message={item.message}
                            author={item.author}
                            date={item.datetime}
                        />
                    )
                })}
            </div>
        </div>
        </>
    );
};

export default Messages;