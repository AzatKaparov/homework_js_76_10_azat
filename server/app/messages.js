const express = require("express");
const router = express.Router();
const {nanoid} = require('nanoid');
const fileDb = require('../fs/mainFS');

router.get(`/`, (req, res) => {
    const errorObj = {error: "Wrong query datetime!"}
    const messages = fileDb.getItems();
    if (req.query.datetime) {
        const datetime = req.query.datetime;
        const date = new Date(datetime);
        if (isNaN(date.getDate())) {
            res.status(400).send(errorObj);
        } else {
            const sortedData = messages.filter(item => {
                return new Date(item.datetime) > date;
            });
            return res.send(sortedData);
        }
    }
    res.send(messages);
});

router.post('/', (req, res) => {
    const errorObj = {"error": "Author and message must be present in the request"};
    if (!req.body.author || req.body.author === "" || req.body.author === undefined || req.body.author === null) {
        return res.status(400).send(errorObj);
    }
    if (!req.body.message || req.body.message === "" || req.body.message === undefined || req.body.message === null) {
        return res.status(400).send(errorObj);
    }
    const date = new Date().toISOString();
    const product = {...req.body, datetime: date, id: nanoid()};
    fileDb.addItem(product);
    res.send(product);
});


module.exports = router;