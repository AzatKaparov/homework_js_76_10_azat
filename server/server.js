const express = require('express');
const cors = require('cors');
const fileDb = require('./fs/mainFS');
const app = express();
const messages = require('./app/messages');
const port = 4321;

fileDb.init();
app.use(express.json());
app.use(cors());
app.use('/messages', messages);

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
